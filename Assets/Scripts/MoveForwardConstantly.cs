﻿using UnityEngine;
using System.Collections;
using System;

public class MoveForwardConstantly : MonoBehaviour
{

    [SerializeField]
    private float acceleration = 100f;

    [SerializeField]
    private float initialVelocity = 10f;

    private Rigidbody2D ourRigidbody;
    
    // Because we're in a Pool, and Start and Awake only ever get called once, we'll use OnEnable instead!
    // @TODO: Verify that we can't GetComponent<Rigidbody2D> in Start or Awake instead;
    public void OnEnable()
    {
        ourRigidbody = GetComponent<Rigidbody2D>();

        ourRigidbody.velocity = Vector2.up * initialVelocity;
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 forceToAdd = Vector2.up * acceleration * Time.deltaTime;

        ourRigidbody.AddForce(forceToAdd);
    }
}
