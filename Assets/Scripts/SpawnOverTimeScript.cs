﻿using UnityEngine;
using System.Collections;

public class SpawnOverTimeScript : MonoBehaviour
{

	[SerializeField]
	private GameObject spawnObject = null;

    // Delay before starting to spawn
    [SerializeField]
    private float initialDelay = 2f;

    // Delay between spawns
    [SerializeField]
    private float spawnDelay = 2f;

    private Renderer ourRenderer;

    // Use this for initialization
    void Start()
    {
        ourRenderer = GetComponent<Renderer>();

        // Stop our Spawner from being visible!
        ourRenderer.enabled = false;

        // Call the given function after initialDelay seconds, 
        // and then repeatedly call it after spawnDelay seconds.
        InvokeRepeating("Spawn", initialDelay, spawnDelay);
    }

    void Spawn()
    {
        float X = transform.position.x - ourRenderer.bounds.size.x / 2;
        float XX = transform.position.x + ourRenderer.bounds.size.x / 2;

        // Randomly pick a point within the spawn object
        Vector2 SPAWNLOC = new Vector2(Random.Range(X, XX), transform.position.y);

        // Spawn the object at the 'spawnLocation' position
		Instantiate(spawnObject, SPAWNLOC, Quaternion.identity);
    }
}
