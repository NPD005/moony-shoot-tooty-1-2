﻿using UnityEngine;
using System.Collections;

public class PlayerMovementScript : MonoBehaviour
{

    // SerializeField exposes this value to the Editor, but not to other Scripts!
    // It is "pseudo public"
    // HorizontalPlayerSpeed indicates how fast we accelerate Horizontally
    [SerializeField]
    private float horizontalPlayerAcceleration = 8000f;

    // SerializeField exposes this value to the Editor, but not to other Scripts!
    // It is "pseudo public"
    // HorizontalPlayerSpeed indicates how fast we accelerate Horizontally
    [SerializeField]
    private float verticalPlayerAcceleration = 4000f;

    private Rigidbody2D ourRigidbody;

    // Use this for initialization
    void Start()
    {
        // Get OurRigidbodyComponent once at the start of the game and store a reference to it
        // This means that we don't need to call GetComponent more than once! This makes our game faster. (GetComponent is SLOW)
        ourRigidbody = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        float horizontalInput = Input.GetAxis("Horizontal");

        if (horizontalInput != 0.0f)
        {
            Vector2 ForceToAdd = Vector2.right * horizontalInput * horizontalPlayerAcceleration * Time.deltaTime;

            ourRigidbody.AddForce(ForceToAdd);

            //print(HorizontalInput);
        }

        float verticalInput = Input.GetAxis("Vertical");

        if (verticalInput != 0.0f)
        {
            Vector2 ForceToAdd = Vector2.up * verticalInput * verticalPlayerAcceleration * Time.deltaTime;

            ourRigidbody.AddForce(ForceToAdd);

            //print(HorizontalInput);
        }
    }
}
